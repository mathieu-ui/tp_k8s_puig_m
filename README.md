# tp_k8s_puig_m

## 1.	Création du namespace

![img1](./img/exo1.png)

## 2.	Création d’un pod

![img2](./img/exo2.png)

## 3.	Création d’un déploiement.

![img3](./img/exo3.png)

## 4.	Autre contrôleur.

![img4](./img/exo4.png)

## 5.	Back-end.

![img5](./img/exo5.png)